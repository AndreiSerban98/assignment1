console.log("assignment works");
function distance(array1, array2){
    try{
        if ((Array.isArray(array1) && Array.isArray(array2)) == false) throw "InvalidType";
        let unique1 = new Set(array1);
        let unique2 = new Set(array2);
        var contor = 0;
        unique1.forEach( e => {
          unique2.forEach( f => {
            if (e === f) {
              contor++;
            }
          })
        })
        contor = unique1.size + unique2.size - contor*2;
        return contor;
    } catch(err) {
        return err;
    }
}
console.log(distance([2], [1,'2',3]));
console.log(distance([ ], [ ]));
console.log(distance([2], 1));
console.log(distance([2,4],[1,2,3]));